const inventory = require("./inventory.js")

const BMWandAudi = inventory.filter( function(car){
    if(car.car_make=="BMW" || car.car_make=="Audi")
        return car
})

module.exports = BMWandAudi; 
