const inventory = require("./inventory");

const sortCarYear = inventory.map((x) => x.car_year).sort()

module.exports = sortCarYear;