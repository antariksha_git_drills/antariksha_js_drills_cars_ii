const sortedCarYear = require("./problem4.js") 
const carYear = sortedCarYear;
const olderThanYear2000 = carYear.reduce(function (count,curr){
    if(curr<2000)
    {
        count++;
    }
    return count
},0)
module.exports = olderThanYear2000;